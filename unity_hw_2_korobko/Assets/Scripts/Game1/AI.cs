using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Base;

namespace Game1
{

[RequireComponent(typeof(Unit))]
public class AI : MonoBehaviour
{
  Unit unit;

  Vector3 move_vector;

  void Awake()
  {
    unit = GetComponent<Unit>();
  }

  CoroutineAnimation coroutine_animation;

  void Start()
  {
    coroutine_animation = GetComponent<CoroutineAnimation>();
    coroutine_animation.StartAnimation(CoroutineAnimation.Animation.SPAWN, 0.2f);
  }

  void Update()
  {
    unit.enabled = !coroutine_animation.IsBusy;

    move_vector = transform.forward;

    unit.Move(move_vector);

    TimeToDie();
  }

  void TimeToDie()
  {
    if(coroutine_animation.IsBusy)
      return;

    if(Mathf.Abs(transform.position.x) > 6.0f || Mathf.Abs(transform.position.z) > 6.0f)
    {
      coroutine_animation.StartAnimation(CoroutineAnimation.Animation.DIE, 0.2f, () => {
        GameObject.Destroy(gameObject);
      });
    }
  }
}

}
