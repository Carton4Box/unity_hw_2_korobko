using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Base;

namespace Game1
{

[RequireComponent(typeof(JamInput))]
public class Game : MonoBehaviour
{
  public Unit hero;

  public Unit enemy;
  public Unit coin;

  JamInput input;
        static int per;
        static float per2;
        static int spawn_green_r_f = 1, spawn_green_r_s = 5;
  int spawn_counter;

  public int spawn_green_after = per;//3
  public int win_green_count = 3;

  public float min_enemy_speed = 1.0f;
  public float max_enemy_speed = 3.0f;

  public GameObject win_label;

  void Start()
  {
    per = Random.Range(spawn_green_r_f, spawn_green_r_s);
            per2 = Random.Range(spawn_r_f, spawn_r_s);
    input = GetComponent<JamInput>();
    spawn_counter = 0;
    win_label.SetActive(false);
  }

  void Update()
  {
    input.Tick();
    hero.Move(new Vector3(input.horizontal, 0.0f, input.vertical));

    if(win_green_count > 0)
      Spawn();
    else
      win_label.SetActive(true);
  }
        static float spawn_r_f = 0.0f, spawn_r_s = 4.0f;
        float time;
  public float spawn_pause = per2;//10

  

  void InitRandomSpawnGreenAfter()
  {
    spawn_green_after = Random.Range(min_spawn_green_after, max_spawn_green_after+1);
  }

  void Spawn()
  {
    if(time >= spawn_pause)
    {
      Unit new_enemy;

      if(spawn_counter < spawn_green_after)
        new_enemy = Instantiate(enemy);
      else
      {
        spawn_counter = 0;
        InitRandomSpawnGreenAfter();
        new_enemy = Instantiate(coin);
        new_enemy.on_coin = OnCoin;
      }

      new_enemy.transform.Rotate(Vector3.up, Random.Range(0, 360));
      new_enemy.transform.Translate(new_enemy.transform.forward * -5);
      new_enemy.transform.forward = (hero.transform.position - new_enemy.transform.position).normalized;
      new_enemy.speed = Random.Range(min_enemy_speed, max_enemy_speed);
      time = 0.0f;
      spawn_counter++;
    }
    time += Time.deltaTime;
  }

  void OnCoin()
  {
    win_green_count--;
  }
}

}
