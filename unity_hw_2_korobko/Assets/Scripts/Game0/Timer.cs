﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace Game0
{
    public class Timer : MonoBehaviour
    {

        public Text second;
        public int sec = 10;

       public void Start()
        {
            InvokeRepeating("RunTimer", 1, 1);
        }

        public void obnov()
        {
            sec = 11;
        }

        public bool slow_game()
        {
            if (sec == 0)
            { return false; }
            return true;
        }

       public bool RunTimer()
        {

            if (sec > 0)
            {
                sec -= 1;
                second.text = sec.ToString();
                return true;
            }
            else if (sec == 0)
            {
                second.text = sec.ToString();
                return false;
            }
            return true;
           

        }

    }
}
