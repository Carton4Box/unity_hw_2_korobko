﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Base;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


namespace Game0
{

[RequireComponent(typeof(JamInput))]
public class Game : MonoBehaviour
{
  public GameObject target_spawn_placeholder;
  public Hero hero;
  public Target origin;
  public Target true_origin;
  public Timer timer;
  public float start_hero_speed = 0.2f;
  public float step_hero_speed = 0.01f;
  public float max_hero_speed = 1.0f;
  public float distance_between_targets = 1.2f;
        public bool param_timer = true;
 

        int initial_win_to_next_scene;
  public int win_to_next_scene = 5;

  List<Target> targets = new List<Target>();

  int target_count = 1;

  public float laser_time = 1.0f;

  enum GameState
  {
    GAME,
    FIRE,
    NEXT_LEVEL,
    RESET,
    WAIT
  }

  GameState state = GameState.GAME;

  JamInput input;

  void Start()
  {
    input = GetComponent<JamInput>();
    SetupGame();
    state = GameState.GAME;
    initial_win_to_next_scene = win_to_next_scene;
  }


  void SetupGame(bool win = true)
  {
    if(!win)
      target_count = 1; //Reset game
    SpawnTargets();
    SetupHeroMovement();
    RandomizeTrueTargetPosition();
   }

  void SetupHeroMovement()
  {
    var speed  = start_hero_speed + step_hero_speed * (targets.Count - 1);
    speed = Mathf.Min(speed, max_hero_speed);
    hero.Setup(targets[0].transform.position.x, targets[targets.Count - 1].transform.position.x, speed);
  }

  void SpawnTargets()
  {
    targets.Clear();

    SpawnTarget(true_origin);

    for(int i = 1; i < target_count; ++i)
      SpawnTarget(origin);

    var start_position_x = target_spawn_placeholder.transform.position.x -
      (distance_between_targets * target_count) / 2.0f;

    for(int i = 0; i < targets.Count && target_count > 1; ++i)
    {
      targets[i].transform.position = new Vector3(
        start_position_x + distance_between_targets * (float)i,
        target_spawn_placeholder.transform.position.y,
        target_spawn_placeholder.transform.position.z);
    }

    target_count++;
  }

  void RandomizeTrueTargetPosition()
  {
    int index = Random.Range(0, targets.Count);
    var true_target_position = targets[0].transform.position;
    targets[0].transform.position = targets[index].transform.position;
    targets[index].transform.position = true_target_position;
  }

  void SpawnTarget(Target origin)
  {
    var new_target = Target.Instantiate(origin);
    new_target.transform.position = target_spawn_placeholder.transform.position;
    targets.Add(new_target);
  }

  bool IsWin()
  {
           
    foreach(var target in targets)
    {
      if(!target.is_true_target)
        continue;

      if(Mathf.Abs(hero.transform.position.x - target.transform.position.x) < 0.6f)
        return true;
    }

    return false;
  }
  
  void ProcessInput()
  {
    if(input.touchBegan)
      state = GameState.FIRE;
  }

  IEnumerator DoFire()
  {
    bool is_win = IsWin();

    hero.SetLaserState(true);
    foreach(var target in targets)
      target.SelfDestroy();
    targets.Clear();
    timer.obnov();//Обновление таймера
    yield return new WaitForSeconds(laser_time);

    hero.SetLaserState(false);
    state = is_win ? GameState.NEXT_LEVEL : GameState.RESET;
    win_to_next_scene = is_win ? --win_to_next_scene : initial_win_to_next_scene;

    if(win_to_next_scene == 0)
      SceneManager.LoadScene("Game1");
  }

  IEnumerator DoSetupGame(bool win = true)
  {
    var hero_animation = hero.GetComponent<CoroutineAnimation>();
    hero_animation.StartAnimation(CoroutineAnimation.Animation.DIE, 0.4f);
    yield return new WaitForSeconds(0.4f);
    SetupGame(win);
    hero_animation.StartAnimation(CoroutineAnimation.Animation.SPAWN, 0.4f);
    yield return new WaitForSeconds(0.4f);
    state = GameState.GAME;
  }

  void Update()
  {
     param_timer = timer.slow_game();
            
     switch (state)
    {
      case GameState.GAME:
        ProcessInput();
        hero.Tick(Time.deltaTime);
        break;
      case GameState.FIRE:
        StartCoroutine(DoFire());
        state = GameState.WAIT;
        break;
      case GameState.NEXT_LEVEL:
        StartCoroutine(DoSetupGame());
        state = GameState.WAIT;
        break;
      case GameState.RESET:
        StartCoroutine(DoSetupGame(win: false));
        state = GameState.WAIT;
        break;
      default:
        break;
    }
  }
}

}
