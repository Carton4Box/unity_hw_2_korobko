using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game0
{

public class Hero : MonoBehaviour
{
  public float x_a;
  public float x_b;

  public float speed = 1.0f;

  public GameObject laser;

  float life_time;

  public void SetLaserState(bool state)
  {
    laser.SetActive(state);
  }

  void Start()
  {
    SetLaserState(false);
  }

  public void Setup(float x_a, float x_b, float speed)
  {
    life_time = 0.5f / speed;

    if(Random.Range(0, 2) > 0)
      life_time += 1.0f / speed;

    this.x_a = x_a;
    this.x_b = x_b;
    this.speed = speed;

    Tick(0.0f);
  }

  public void Tick(float dt)
  {
    life_time += dt;
    transform.position = new Vector3(Mathf.Lerp(x_a, x_b, Mathf.PingPong(life_time * speed, 1.0f)), 0.0f, 0.0f);
  }
}

}
